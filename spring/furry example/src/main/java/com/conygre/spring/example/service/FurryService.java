package com.conygre.spring.example.service;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;


@Service
public interface FurryService {

    Collection<Furry> getFurries();
    
    void addFurry(Furry furry);

	void deleteFurry(ObjectId objectId);

    void updateFurry(Furry furry);
    // update if name matched

}