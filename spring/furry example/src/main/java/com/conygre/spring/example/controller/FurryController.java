package com.conygre.spring.example.controller;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;
import com.conygre.spring.example.service.FurryService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/furries")    //set what URL we want to show
public class FurryController {
    @Autowired
    private FurryService service;
    
    @RequestMapping(method = RequestMethod.GET)
    public Collection<Furry>getFurries(){
        return service.getFurries();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addFurry(@RequestBody Furry furry){
        service.addFurry(furry);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateFurry(@RequestBody Furry furry){
        service.updateFurry(furry);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteFurry(@PathVariable("id") String id) {
		service.deleteFurry(new ObjectId(""+id));
	}

	
}