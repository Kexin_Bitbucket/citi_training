package com.conygre.spring.example.repo;

import com.conygre.spring.example.entities.Furry;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface FurryRepository extends MongoRepository<Furry,ObjectId>{

}