package com.conygre.spring.example.service;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;
import com.conygre.spring.example.repo.FurryRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class FurryServiceImpl implements FurryService {
    @Autowired
    private FurryRepository repo;
    @Autowired
    private MongoTemplate mongoTemplate;



    @Override
    public void updateFurry(Furry furry) {
    Query query = Query.query(Criteria.where("name").is(furry.getName()));
    Update update = Update.update("animal",furry.getAnimal());
    mongoTemplate.updateMulti(query, update, Furry.class);
    }


    @Override
    public Collection<Furry> getFurries() {
        return repo.findAll();
    }

    @Override
    public void addFurry(Furry furry) {
        repo.insert(furry);

    }

    @Override
    public void deleteFurry(ObjectId id) {
        repo.deleteById(id);

    }


    
    
}