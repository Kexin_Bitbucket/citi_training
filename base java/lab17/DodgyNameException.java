public class DodgyNameException  extends Exception{

    public DodgyNameException(String message) {
        super(message);
    }
    
    
    @Override
	public String toString() {
		return "DodgyNameException: " + getMessage();
	}
    
}