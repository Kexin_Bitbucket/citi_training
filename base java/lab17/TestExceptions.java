import java.util.HashSet;

public class TestExceptions {
    public static void main(String[] args) {
        HashSet<Account> accounts;
            accounts = new HashSet<Account>();

        try{
            Account ac1 = new CurrentAccount(10, "name1");
            Account ac2 = new CurrentAccount(10, "name2");
            Account ac3 = new SavingsAccount(30, "name3");

            accounts.add(ac1);
            accounts.add(ac2);
            accounts.add(ac3);


        }catch(DodgyNameException e){
            System.out.println(e.toString());
            return; //which will end the main method
        }finally{
            double tax = 0;
            for (Account ac : accounts) {
                tax += ac.getBalance()*0.4;
                
            }
            System.out.println(tax);
            // if no account named "Fingers", then we get total tax from each
            // else, get zero total tax, zero from other "good" accounts as well
        }
    }
}