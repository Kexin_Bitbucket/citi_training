public abstract class Account {
    // properties
    private double balance;
    private String name;

    // methods
    public void addInterest() {

    }

    public Account(double balance, String name) throws DodgyNameException {
        if ("Fingers".equals(name)){
            throw new DodgyNameException("Fingers is not allowed an account!");
        }else{
        this.name = name;
        this.balance = balance;
        }
    }







    // getters and setters

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws DodgyNameException {
        if ("Fingers".equals(name)){
            throw new DodgyNameException("Fingers is not allowed an account!");
        }else{
        this.name = name;
        }
    }


    
}