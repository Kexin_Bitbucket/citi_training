public class SavingsAccount extends Account {
    // properties - as Account

    // methods
    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.4);
    }

    public SavingsAccount(double balance, String name) throws DodgyNameException{
        super(balance,name);
    }
}