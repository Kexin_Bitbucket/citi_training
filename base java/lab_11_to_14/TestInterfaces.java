public class TestInterfaces {
    public static void main(String[] args) {
        Detailable ac1 = new SavingsAccount(34, "name1");
        Detailable ac2 = new CurrentAccount(23, "name1");
        Detailable ac3 = new HomeInsurance(324, 56, 35646.344);

        Detailable[] detailablesArray = {ac1,ac2,ac3};

        //use polymorphism to call the appropriate getDetails()
        for (Detailable ac : detailablesArray) {
            System.out.println(ac.getDetails());
        }
    }
}