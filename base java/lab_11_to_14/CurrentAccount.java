public class CurrentAccount extends Account{
    //properties - as Account

    //methods
    @Override
    public void addInterest(){
        setBalance(getBalance() *  1.1);
    }

    public CurrentAccount(double balance,String name){
        super(balance,name);
    }


}