public class TestLambdas {
    public static void main(String[] args) {
        Detailable det = () -> { return "hello world";};
        System.out.println(det.getDetails());
        
        // like the exmaple in class
        doThis(() -> {return "you did this";});
    }
    
    // like the exmaple in class
    private static void doThis (Detailable detail){
            System.out.println(detail.getDetails());
        }
}