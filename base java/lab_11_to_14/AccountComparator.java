public class AccountComparator implements java.util.Comparator <Account> {
    //must return a int
    @Override
    public int compare(Account a1, Account a2) {

        if (a1.getBalance() < a2.getBalance())
            return -1;
        if (a1.getBalance() > a2.getBalance()) 
            return 1;
        else return 0;
    }
}