import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;


public class CollectionsTest {
    public static void main(String[] args) {

        // phase 1: organizing accounts in a HashSet ----------------------
        HashSet<Account> accounts;
        accounts = new HashSet<Account>();

        Account ac1 = new CurrentAccount(2, "name1");
        Account ac2 = new CurrentAccount(4, "name2");
        Account ac3 = new SavingsAccount(6, "name3");

        accounts.add(ac1);
        accounts.add(ac2);
        accounts.add(ac3);

        // 1.1 iterator approach
        System.out.println("using ierator approach ===============================");

        Iterator<Account> accountIter = accounts.iterator();
        while(accountIter.hasNext()){
            Account thisAccount = accountIter.next();
            System.out.println("name = " + thisAccount.getName() + "  balance = " + thisAccount.getBalance() + "  the class type is " + thisAccount.getClass());
            thisAccount.addInterest();
            System.out.println("updated balance = " + thisAccount.getBalance());
            
        }

        System.out.println("using for each approach ===============================");


        // 1.2 for each approach
        for (Account a: accounts) {  
            System.out.println("name = " + a.getName()  + "  balance = " + a.getBalance() + "  the class type is " + a.getClass());
            a.addInterest();
            System.out.println("updated balance = " + a.getBalance());
            
        }









        // phase 2: organizing accounts in a TreeSet --------------------
        TreeSet<Account> accountTree;
        accountTree = new TreeSet<Account>(new AccountComparator());

        //reset the value
        ac1 = new CurrentAccount(45, "name1");
        ac2 = new CurrentAccount(67, "name2");
        ac3 = new SavingsAccount(6, "name3");

        accountTree.add(ac1);
        accountTree.add(ac2);
        accountTree.add(ac3);

        // show that the accounts in the tre is sorted
        System.out.println("phase 2: using TreeSet ================================================");
        for (Account ac : accountTree) {
            System.out.println(" name = " + ac.getName() + " balance = " + ac.getBalance());
            
        }
            
    }

}
