public abstract class Account implements Detailable {
    // properties
    private double balance;
    private String name;

    // methods
    public void addInterest() {

    }

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    @Override
    public String getDetails() {
        return "balance = " + balance + " name = " + name;
    }






    // getters and setters

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    
}