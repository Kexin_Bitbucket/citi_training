public class TestInheritance {
    public static void main(String[] args) {
        // Acount ac1 = new Account(2,"name1");
        // stay as general as possible, so declare them as Account? 
        Account ac2 = new SavingsAccount(4, "name2");
        // System.out.println(ac2.getClass()); //shows runtime class of this object
        Account ac3 = new CurrentAccount(6, "name3");
        Account[] accountList = {ac2, ac3};

        for (int i=0; i < accountList.length; i++){

            System.out.println(accountList[i].getClass() +" with account name = " +  accountList[i].getName() + "\nbefore, banlance = " + accountList[i].getBalance());
            accountList[i].addInterest();
            System.out.println("after, banlance = " + accountList[i].getBalance());
        }


    }
}