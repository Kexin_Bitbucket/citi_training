import java.util.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class TestStrings {
    public static void main(String[] args) {
        int[] a = {1,2,3};
        int[] b = a;
        int[] c = a.clone();
        a[0] = 5;
        System.out.println(Arrays.toString(b)); // b = [5,2,3]
        System.out.println(Arrays.toString(c)); // c = [1,2,3]

        // lab 10 - part 1
        // 2.
        String s = "example.doc";
        s.replace("doc", "bak");
        System.out.println(s);

        // 3.
        String s1 = "abcde";
        String s2 = "fghijklmn";
         //comparing two strings lexicographically
        System.out.println(s1.compareTo(s2));
        // return -5, so s1 is lexicographically smaller than s2


        // 4.
        String text = "the quick brown fox swallowed down the lazy chicken";
        String findText = "ow";
        int count = 0;
        int index = 0;
        // find the next "ow" until there is none
        while((index = text.indexOf(findText, index)) != -1){
            index++;
            count++;
        }
        System.out.println("count = "+ count);

        // 5.
        String s3 = "Live not on evil";
        // remove all spaces, and change to lowercase
        s3 = s3.toLowerCase().replace(" ", "");
        StringBuilder s4 = new StringBuilder(s3);
        String s5 = new String(s4.reverse());
        System.out.println("string is a palindrome = " + s3.equals(s5));  



        //lab 10 - part 2
        System.out.println("lab 10 - part 2================================");
        Format dayMonthYear = new SimpleDateFormat("dd MM yyyy");
        Format time = new SimpleDateFormat("yyy/mm/dd - hh:mm:ss"); //yyy same result as yyyy?
        
        System.out.println(dayMonthYear.format(new Date()));//current time
        System.out.println(time.format(new Date()));


        // lab 10 - part 3
        text = "Part 3 Optional Splitting Text?\n"
        + "Copy a page of news or other text from your favourite website. "
        + "Split the text into sentences, count and display them! "
        + "Next split each sentence into words, count and display the words. "
        + "Finally split the whole text into words and count them, checking that this is consistent with the results of the previous step.";
        
        String[] sentanceSplit = text.split("\\!|\\?|\\.");
        System.out.println("sentanceSplit count = " + sentanceSplit.length);
        // System.out.println(sentanceSplit[4]);
        
        int countWord = 0;
        for (String str : sentanceSplit){
            countWord += str.split(" ").length;
        }
        System.out.println("paraSplit then wordSplite count = " + countWord);

        String[] wordSplit= text.split(" |\\!|\\?|\\.");
        System.out.println("word count = " + wordSplit.length);
   
   
   
   
   
   
    }
}