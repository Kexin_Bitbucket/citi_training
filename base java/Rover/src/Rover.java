public class Rover {
    // proterties
    private int direction; // 0 north, 1 east, 2 south, 3 west
    private int x;
    private int y;
    //private int stepsToGo;
    private StringBuilder nextSteps;  

    {
        x = 0;
        y = 0;
        // stepsToGo = 2;
        nextSteps = new StringBuilder();
    }




    //methods

    public void receiveCommands(String command){
        for (char aChar : command.toCharArray()){
            if (aChar == 'L' || aChar == 'R'){
                nextSteps.append(aChar);
            }else{
                int numMoves = Character.getNumericValue(aChar);
                // System.out.println("numMoves = " + numMoves);
                for (int i=0; i<numMoves; i++){
                    nextSteps.append('M');
                }
            }
        }
        System.out.println("processed nextSteps = " + nextSteps);
    }


    public void takeNextStep(){
        // System.out.println("takeNextStep-------------");
        if (nextSteps.length() != 0){
            char nextStep = nextSteps.charAt(0);
            nextSteps.deleteCharAt(0);
            // System.out.println("nextstep =" + nextStep);
        
            
            if (nextStep == 'L'){
                // left turn, direction -1, but till want it in 1-4
                direction = (direction + 3) % 4;
            }else if(nextStep == 'R'){
                // left turn, direction +1
                direction = (direction + 1) % 4;
            }else if(nextStep == 'M'){
                // move one unit in current direction
                // System.out.println("direction before move = " + direction);
                switch(direction){
                    case 0: //north
                        y += 1;
                        break;
                    case 1: //east
                        x += 1;
                        break;
                    case 2: //south
                        y -= 1;
                        break;
                    case 3: //west
                        x -= 1;
                        break;
            
                }    
            }
        }
    }
        




    public Boolean isBusy(){
        if(nextSteps.length() != 0){
            return true;
        }else{
            return false;
        }
    }










    // getters and setters

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }



}